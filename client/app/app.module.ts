import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import {
  MatButtonModule, MatToolbar, MatToolbarModule, MatIconModule,
  MatCardModule, MatFormFieldModule, MatInputModule, MatSpinner,
  MatProgressSpinnerModule, MatSnackBarModule, MatButtonToggleModule, MatGridListModule, MatDatepickerModule,
  MatNativeDateModule, MatTooltipModule, MatListModule, MatExpansionModule, MatSelectModule, MatMenuModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgRedux, DevToolsExtension, NgReduxModule } from '@angular-redux/store';
import { IAppState } from '../store/IAppState';
import { NgReduxRouter, NgReduxRouterModule } from '@angular-redux/router';
import { RootEpics } from '../store/root.epic';
import { rootReducer } from '../store/root.reducer';
import { environment } from '../environments/environment';
import { createLogger } from 'redux-logger';
import { provideReduxForms } from '@angular-redux/form';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { LoginService } from './core/login/login.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppContainerComponent } from './components/app-container/app-container.component';
import { LoginEpics } from '../store/login/login.epic';
import { LoginActions } from '../store/login/login.actions';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from './core/http-interceptors/http-error-interceptor';
import { XsfrHttpInterceptor } from './core/http-interceptors/http-xsfr-interceptor';
import { UserDataActions } from '../store/user/user-data.actions';
import { UserDataService } from './core/user-data/user-data.service';
import { UserDataEpics } from '../store/user/user-data.epic';
import { FirstLoginComponent } from './components/first-login/first-login.component';
import { LoginGuard } from './components/login/login.guard';
import { FirstLoginGuard } from './components/first-login/first-login.guard';
import { ChangePasswordEpics} from '../store/change-password/change-password.epic';
import { ChangePasswordActions} from '../store/change-password/change-password.actions';
import { ChangePasswordService} from './core/change-password/change-password.service';
import { MonthViewComponent } from './components/month-view/month-view.component';
import { WeekViewComponent } from './components/week-view/week-view.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { AddUserGuard } from './components/add-user/add-user.guard';
import { AddUserActions } from '../store/add-user/add-user.actions';
import { AddUserEpic } from '../store/add-user/add-user.epic';
import {EventService} from "./core/events/event.service";
import {EventActions} from "../store/event/event.actions";
import {EventEpic} from "../store/event/event.epic";
import { EventViewComponent } from './components/event-view/event-view.component';
import { EventViewMonthComponent } from './components/event-view-month/event-view-month.component';
import { EventManipulationComponent } from './components/event-manipulation/event-manipulation.component';
import {ColleaguesEpic} from "../store/colleagues/colleagues.epic";
import {ColleaguesActions} from "../store/colleagues/colleagues.actions";
import {LogoutEpic} from "../store/logout/logout.epic";
import {LogoutActions} from "../store/logout/logout.actions";
import {InvitationsActions} from "../store/invitations/invitations.actions";
import {InvitationsEpic} from "../store/invitations/invitations.epic";
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'app', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'app', component: AppContainerComponent, canActivate: [LoginGuard] },
  { path: 'firstLogin', component: FirstLoginComponent, canActivate: [LoginGuard, FirstLoginGuard]},
  { path: 'add-user', component: AddUserComponent, canActivate: [LoginGuard, AddUserGuard]},
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AppContainerComponent,
    FirstLoginComponent,
    MonthViewComponent,
    WeekViewComponent,
    AddUserComponent,
    EventViewComponent,
    EventViewMonthComponent,
    EventManipulationComponent,
    PageNotFoundComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgReduxModule,
    NgReduxRouterModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'PYAToken',
      headerName: 'Authorization',
    }),
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatButtonToggleModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatListModule,
    MatExpansionModule,
    MatSelectModule,
    MatMenuModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: XsfrHttpInterceptor,
      multi: true,
    },
    RootEpics,
    LoginEpics,
    UserDataEpics,
    ChangePasswordEpics,
    AddUserEpic,
    EventEpic,
    ColleaguesEpic,
    InvitationsEpic,
    LogoutEpic,
    LoginActions,
    UserDataActions,
    ChangePasswordActions,
    AddUserActions,
    EventActions,
    InvitationsActions,
    ColleaguesActions,
    LogoutActions,
    LoginService,
    UserDataService,
    ChangePasswordService,
    EventService,
    LoginGuard,
    FirstLoginGuard,
    AddUserGuard
  ],
  entryComponents: [EventViewComponent, EventViewMonthComponent, EventManipulationComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    public store: NgRedux<IAppState>,
    devTools: DevToolsExtension,
    ngReduxRouter: NgReduxRouter,
    rootEpics: RootEpics
  ) {
    store.configureStore(
      rootReducer,
      {},
      environment.production ? [...rootEpics.createEpics()] : [createLogger(), ...rootEpics.createEpics()],
      devTools.isEnabled() ? [devTools.enhancer()] : []
    );
    if (ngReduxRouter) {
      ngReduxRouter.initialize();
    }
    provideReduxForms(store);
  }
}
