import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventViewMonthComponent } from './event-view-month.component';

describe('EventViewMonthComponent', () => {
  let component: EventViewMonthComponent;
  let fixture: ComponentFixture<EventViewMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventViewMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventViewMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
