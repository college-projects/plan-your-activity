import { combineReducers } from 'redux';
import { routerReducer } from '@angular-redux/router';
import { loginReducer } from './login/login.reducer';
import { userDataReducer } from './user/user-data.reducer';
import { changePasswordReducer } from './change-password/change-password.reducer';
import { addUserReducer } from './add-user/add-user.reducer';
import { eventReducer } from './event/event.reducer';
import {colleaguesDataReducer} from "./colleagues/colleagues.reducer";
import {logoutReducer} from "./logout/logout.reducer";
import {invitationReducer} from "./invitations/invitations.reducer";

export const rootReducer = combineReducers({
    addUserData: addUserReducer,
    changePasswordData: changePasswordReducer,
    colleagues: colleaguesDataReducer,
    events: eventReducer,
    invitations: invitationReducer,
    loginData: loginReducer,
    logoutData: logoutReducer,
    router: routerReducer,
    userData: userDataReducer
});
