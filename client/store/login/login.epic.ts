import { Injectable } from '@angular/core';
import { createEpicMiddleware } from 'redux-observable';
import { LoginActions } from './login.actions';
import { LoginService } from '../../app/core/login/login.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginEpics {

    constructor(private loginService: LoginService,
                private loginActions: LoginActions) {}

    public createEpic() {
        return createEpicMiddleware(this.loginEpic());
    }

    private loginEpic() {
        return action$ => action$
        .ofType(LoginActions.LOGIN_STARTED)
        .switchMap(action => this.loginService.logIn(action.payload)
            .map(data => this.loginActions.loginSucceeded(data['OK']))
            .catch(data => Observable.of(this.loginActions.loginFailed(data)))
        );
    }
}
