export interface ILogOutData {
    loggedOut: boolean;
    loading: boolean;
    error: any;
}
