import { Injectable } from '@angular/core';
import { createEpicMiddleware } from 'redux-observable';
import { LogoutActions } from './logout.actions';
import { LoginService } from '../../app/core/login/login.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LogoutEpic {

    constructor(private loginService: LoginService,
                private logOutActions: LogoutActions) {}

    public createEpic() {
        return createEpicMiddleware(this.loginEpic());
    }

    private loginEpic() {
        return action$ => action$
        .ofType(LogoutActions.LOGOUT_STARTED)
        .switchMap(action => this.loginService.logOut()
            .map(data => this.logOutActions.logoutSucceeded(true))
            .catch(data => Observable.of(this.logOutActions.logoutFailed(data)))
        );
    }
}
