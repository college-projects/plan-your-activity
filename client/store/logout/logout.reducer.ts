import { ILogOutData } from './logout-data';
import { IPayloadAction } from '../IPayloadAction';
import { LogoutActions } from './logout.actions';
import { State } from '@angular-redux/form/dist/source/state';

const INITIAL_STATE: ILogOutData = {
    loggedOut: false,
    loading: false,
    error: null,
};

export function logoutReducer(state: ILogOutData = INITIAL_STATE, action: IPayloadAction<any, string>) {
    switch (action.type) {
        case LogoutActions.LOGOUT_STARTED : {
            return State.assign(state, [], {
                loggedOut: false,
                loading: true,
                error: null,
            });
        }
        case LogoutActions.LOGOUT_SUCCEEDED: {
            return State.assign(state, [], {
                loggedOut: action.payload,
                loading: false,
                error: null,
            });
        }
        case LogoutActions.LOGOUT_FAILED: {
            return State.assign(state, [], {
                loggedOut: false,
                loading: false,
                error: action.error,
            });
        }
        default: {
            return state;
        }
    }
}
