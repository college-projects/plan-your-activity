export interface IAddUserData {
    added: boolean;
    loading: boolean;
    error: any;
}
