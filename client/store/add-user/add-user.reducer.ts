import { IAddUserData } from './add-user-data';
import { IPayloadAction } from '../IPayloadAction';
import { AddUserActions } from './add-user.actions';
import { State } from '@angular-redux/form/dist/source/state';

const INITIAL_STATE: IAddUserData = {
    added: false,
    loading: false,
    error: null,
};

export function addUserReducer(state: IAddUserData = INITIAL_STATE, action: IPayloadAction<any, string>) {
    switch (action.type) {
        case AddUserActions.ADD_STARTED : {
            return State.assign(state, [], {
                added: false,
                loading: true,
                error: null,
            });
        }
        case AddUserActions.ADD_SUCCEDED: {
            return State.assign(state, [], {
                added: action.payload,
                loading: false,
                error: null,
            });
        }
        case AddUserActions.ADD_FAILED: {
            return State.assign(state, [], {
                added: false,
                loading: false,
                error: action.error,
            });
        }
        default: {
            return state;
        }
    }
}
