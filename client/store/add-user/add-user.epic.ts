import { Injectable } from '@angular/core';
import { createEpicMiddleware } from 'redux-observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';
import { UserDataService } from '../../app/core/user-data/user-data.service';
import { AddUserActions } from './add-user.actions';

@Injectable()
export class AddUserEpic {

    constructor(private userDataService: UserDataService,
                private addUserActions: AddUserActions) {}

    public createEpic() {
        return createEpicMiddleware(this.loginEpic());
    }

    private loginEpic() {
        return action$ => action$
        .ofType(AddUserActions.ADD_STARTED)
        .switchMap(action => this.userDataService.createUser(action.payload)
            .map(data => this.addUserActions.addingSucceded(data['OK']))
            .catch(data => Observable.of(this.addUserActions.addingFailed(data)))
        );
    }
}
