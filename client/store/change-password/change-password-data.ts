export interface IChangePasswordData {
    changed: boolean;
    loading: boolean;
    error: any;
}
