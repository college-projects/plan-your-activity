import * as express from 'express';
import {ServerLoader, IServerLifecycle, ServerSettings} from "ts-express-decorators";
import {Exception} from "ts-httpexceptions";
import * as path from "path"

const bodyParser = require('body-parser');

@ServerSettings({
    rootDir: path.resolve(__dirname),
    acceptMimes: ["application/json"],
    debug: false
})
export class Server extends ServerLoader implements IServerLifecycle {
    constructor(){
        super();
        const applicationPath: string = path.resolve(__dirname);
        this.mount('',`${applicationPath}/**/**.js`).createHttpServer(process.env.PORT || 3000);
        this.settings.set('')
    }

    public $onMountingMiddlewares():void | Promise<any>{
        this.use(bodyParser.json(), bodyParser.json({ type: 'application/vnd.api+json' }));
        this.use(express.static(`${__dirname}/../`));
        return null;
    }

    public $onServerInitError(error: any): void {
        console.log(error);
    }
    public $onError(error: any, request: express.Request, response: express.Response, next: Function): void {
        if (response.headersSent) {
            return next(error);
        }
    
        if (typeof error === 'string') {
            response.status(404).send(error);
            return next();
        }
    
        if (error instanceof Exception) {
            response.status(error.status).send(error.message);
            return next();
        }
    
        if (error.name === 'CastError' || error.name === 'ObjectID' || error.name === 'ValidationError') {
            response.status(400).send('Bad Request');
            return next();
        }
    
        response.status(error.status || 500).send('Internal Error');
        return next();
    }
        
}

new Server().start();