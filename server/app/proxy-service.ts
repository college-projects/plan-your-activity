import { Service } from "ts-express-decorators";
import { parse } from "url";
import { environement } from "./proxy-utils";
import * as proxy from 'express-http-proxy';
import * as moment from 'moment';

@Service()
export class ProxyService {
    public handleProxy;
    public handleLoginProxy;
    public handleBodyUpdateProxy;
    constructor() {
      this.handleProxy = (urlPrefix?: string) => proxy(`${environement.API_PROXY_ADDRESS}` ,{
        proxyReqPathResolver: function(req) {
          let path = parse(req.url).path;
          return urlPrefix ? urlPrefix + path : path;
        },
        proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
          return new Promise(function(resolve, reject) {
            resolve(proxyReqOpts);
          })
        }
      });
      this.handleLoginProxy = proxy(`${environement.API_PROXY_ADDRESS}`, {
        proxyReqPathResolver: function(req) {
          let path = parse(req.url).path;
          return path;
        },
        proxyReqBodyDecorator: function(bodyContent, srcReq) {
          const newBody = JSON.parse(Buffer.from(bodyContent.bodyValue, 'base64').toString())
          return newBody;
        },
        proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
          return new Promise(function(resolve, reject) {
            resolve(proxyReqOpts);
          });
        },
        userResDecorator: function(proxyRes, proxyResData, userReq, userRes) {
          if(!proxyResData.toString('utf8')) return JSON.stringify({OK:false});
          userRes.append('Access-Control-Allow-Credentials',true);
          userRes.cookie('PYAToken', proxyResData.toString(),{expires: new Date(Number(new Date())+30*60*1000), httpOnly: false });
          return JSON.stringify({OK:true});
        }
      });
      this.handleBodyUpdateProxy = (urlPrefix?: string) => proxy(`${environement.API_PROXY_ADDRESS}` ,{
        proxyReqPathResolver: function(req) {
          let path = parse(req.url).path;
          return urlPrefix ? urlPrefix + path : path;
        },
        proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
          return new Promise(function(resolve, reject) {
            resolve(proxyReqOpts);
          });
        },
        userResDecorator: function(proxyRes, proxyResData, userReq, userRes) {
          if (!proxyResData.toString('utf8')) {
            return JSON.stringify({OK: false});
          }
          return JSON.stringify({OK: true});
        }
      });
    }
}
