import {Controller, Get, Post, Put} from "ts-express-decorators";
import {ProxyService} from "../proxy-service";
import * as express from "express";

@Controller('/events')
export class EventController {
  constructor(private proxyService: ProxyService) {}

  @Get('')
  retrieveEvents(request: express.Request, response: express.Response, next: Function) {
    return this.proxyService.handleProxy('/service/events')(request, response, next);
  }

  @Get('/invitations')
  retrieveInvitations(request: express.Request, response: express.Response, next: Function) {
    return this.proxyService.handleProxy('/service')(request, response, next);
  }

  @Put('/addEvent')
  addEvent(request: express.Request, response: express.Response, next: Function) {
    return this.proxyService.handleProxy('/service')(request, response, next);
  }

  @Put('/respond')
  acceptInvitation(request: express.Request, response: express.Response, next: Function) {
    return this.proxyService.handleBodyUpdateProxy('/service')(request, response, next);
  }

  @Put('/updateEvent')
  updateEvent(request: express.Request, response: express.Response, next: Function) {
    return this.proxyService.handleProxy('/service')(request, response, next);
  }

  @Put('/deleteEvent')
  deleteEvent(request: express.Request, response: express.Response, next: Function) {
    return this.proxyService.handleBodyUpdateProxy('/service')(request, response, next);
  }

}
