import { Controller } from "ts-express-decorators";
import { UserController } from "./user/user-controller";
import { EventController } from "./event/event-controller";

@Controller('/api', UserController, EventController)
export class ApiController {
    constructor() {}
}
