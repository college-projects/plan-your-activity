import { Controller, Get, Post } from "ts-express-decorators";
import { ProxyService } from "../proxy-service";
import * as express from "express"

@Controller('/user')
export class UserController {
    constructor(private proxyService: ProxyService){}

    @Post('/login')
    doLogin(request: express.Request, response: express.Response, next: Function) {
        return this.proxyService.handleLoginProxy(request, response, next);
    }

    @Get('/details')
    getProfile(request: express.Request, response: express.Response, next: Function) {
       return this.proxyService.handleProxy('/service')(request, response, next);
    }

    @Post('/firstLogin')
    updatePassword(request: express.Request, response: express.Response, next: Function) {
      return this.proxyService.handleBodyUpdateProxy('/service')(request, response, next);
    }

    @Post('/register')
    registerUser(request: express.Request, response: express.Response, next: Function) {
      return this.proxyService.handleBodyUpdateProxy('/service')(request, response, next);
    }

    @Post('/colleagues')
    retrieveColleagues(request: express.Request, response: express.Response, next: Function) {
      return this.proxyService.handleProxy('/service')(request, response, next);
    }

    @Post('/logout')
    logOut(request: express.Request, response: express.Response, next: Function) {
      return this.proxyService.handleBodyUpdateProxy('/service')(request, response, next);
    }
}
