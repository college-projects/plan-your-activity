import {Controller, Get} from "ts-express-decorators";
import * as express from "express";
import * as Path from "path";

@Controller("")
export class RootControler {
    @Get("/*")
    loadIndex(request: express.Request, response: express.Response, next: Function){
        response.sendFile(Path.resolve(__dirname + '/../../index.html'));
    }
}